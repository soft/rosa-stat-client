from datetime import datetime
import fanotify as _fanotify
import mailer
import rpm_utils
import stats_db
import reporter
import daemonize


def main():
    daemonize.daemonize('/tmp/rosa_stat.pid')
    db = stats_db.StatsDB()
    fanotify = _fanotify.Fanotify()
    fanotify.watch(_fanotify.FAN_MARK_ADD | _fanotify.FAN_MARK_MOUNT, _fanotify.FAN_ACCESS, '/bin')
    fanotify.watch(_fanotify.FAN_MARK_ADD | _fanotify.FAN_MARK_MOUNT, _fanotify.FAN_ACCESS, '/sbin')
    fanotify.watch(_fanotify.FAN_MARK_ADD | _fanotify.FAN_MARK_MOUNT, _fanotify.FAN_ACCESS, '/usr/bin')
    fanotify.watch(_fanotify.FAN_MARK_ADD | _fanotify.FAN_MARK_MOUNT, _fanotify.FAN_ACCESS, '/usr/sbin')
    for event in fanotify:
        assert isinstance(event, _fanotify.FanotifyEvent)
        package_name = rpm_utils.get_package_name(event.filename)
        event.close()
        if package_name is not None:
            db.add_packages((package_name,))
        reporter.send_report(db)

if __name__ == '__main__':
    main()