import time
import datetime
import mailer
import rpm_utils


def join_with_project(lst):
    return (package + '||' + rpm_utils.get_project_name(package) for package in lst)


def get_report_text(db, dt):
    return str(int(time.mktime(dt.timetuple()))) + '\n' + '\n'.join(join_with_project(db.get_packages_for_date(dt)))


def send_report(db):
    one_day_delta = datetime.timedelta(days=1)
    dt_last = db.get_last_sent() + one_day_delta
    dt_now = datetime.datetime.now()
    dt_now = datetime.date(dt_now.year, dt_now.month, dt_now.day)
    try:
        while dt_last < dt_now:
            mailer.send_mail(get_report_text(db, dt_last))
            db.update_sent(dt_last)
            dt_last += one_day_delta
        db.delete_older(dt_now)
    except:
        pass