import os

UMASK = 0
WORKDIR = '/'
MAXFD = 1024

if hasattr(os, 'devnull'):
    DEVNULL = os.devnull
else:
    DEVNULL = '/dev/null'


def daemonize(pidfile=None):
    try:
        pid = os.fork()
    except OSError, e:
        raise Exception("%s [%d]" % (e.strerror, e.errno))

    if pid == 0:
        os.setsid()

        try:
            pid = os.fork()
        except OSError, e:
            raise Exception("%s [%d]" % (e.strerror, e.errno))

        if pid != 0:
            os._exit(0)
    else:
        os._exit(0)

    os.chdir(WORKDIR)
    os.umask(UMASK)

    import resource

    maxfd = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
    if maxfd == resource.RLIM_INFINITY:
        maxfd = MAXFD

    for fd in xrange(maxfd):
        try:
            os.close(fd)
        except OSError:
            pass

    os.open(DEVNULL, os.O_RDWR)
    os.dup2(0, 1)
    os.dup2(0, 2)

    if pidfile is not None:
        with open(pidfile, 'w') as f:
            f.write(str(os.getpid()))