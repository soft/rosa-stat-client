import sqlite3
import time
import datetime


class StatsDB(object):
    def __init__(self):
        self.con = sqlite3.connect('stats.db')
        self.cur = self.con.cursor()

    def add_packages(self, packages):
        cur = self.cur
        cur.executemany("INSERT OR IGNORE INTO STATS (DATE, PACKAGE) VALUES (strftime('%s', 'now', 'start of day'), ?)",
                        [(x,) for x in packages])
        self.con.commit()

    def get_packages_for_date(self, date):
        assert isinstance(date, datetime.datetime)
        cur = self.cur
        cur.execute("SELECT PACKAGE FROM STATS WHERE DATE = strftime('%s', ?, 'unixepoch', 'start of day')",
                    (int(time.mktime(date.timetuple())),))
        return [x[0] for x in cur.fetchall()]

    def delete_older(self, date):
        assert isinstance(date, datetime.datetime)
        cur = self.cur
        cur.execute("DELETE FROM STATS WHERE DATE < strftime('%s', ?, 'unixepoch', 'start of day')",
                    (int(time.mktime(date.timetuple())),))

    def update_sent(self, date):
        assert isinstance(date, datetime.datetime)
        cur = self.cur
        cur.execute("DELETE FROM SENT")
        cur.execute("INSERT INTO SENT VALUES (strftime('%s', ?, 'unixepoch', 'start of day'))",
                    (int(time.mktime(date.timetuple())),))

    def get_last_sent(self):
        cur = self.cur
        cur.execute("SELECT DATE FROM SENT")
        res = cur.fetchall()
        if len(res) > 0:
            return datetime.datetime.fromtimestamp(int(res[0][0]))
        else:
            self.update_sent(datetime.datetime.now() - datetime.timedelta(days=1))
            return self.get_last_sent()