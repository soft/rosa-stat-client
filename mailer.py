import smtplib
import config


def send_mail(text):
    header = 'Content-type: text/plain; charset=utf-8\n' + \
             'From: ' + config.EMAIL_FROM + '\n' + \
             'To: ' + ','.join(config.EMAIL_RECEIVERS) + '\n' + \
             'Subject: ' + config.EMAIL_SUBJECT + '\n\n'
    body = header + text.encode('utf8')

    smtpObj = smtplib.SMTP(config.SMTP_HOST, config.SMTP_PORT)
    smtpObj.ehlo()
    smtpObj.starttls()
    smtpObj.ehlo()
    smtpObj.login(config.EMAIL_FROM, config.EMAIL_PASSWORD)
    smtpObj.sendmail(config.EMAIL_FROM, config.EMAIL_RECEIVERS, body)
    smtpObj.quit()