import subprocess


def get_package_name(filename):
    proc = subprocess.Popen(['rpm', '-qf', filename], stdout=subprocess.PIPE)
    data = proc.communicate()[0]
    if proc.wait() == 0:
        return data.strip()


def get_project_name(package_name):
    package_name = '-'.join(package_name.split('-')[:-3])
    proc = subprocess.Popen(['rpm', '-q', '--qf=%{SOURCERPM}', package_name], stdout=subprocess.PIPE)
    data = proc.communicate()[0]
    if proc.wait() == 0:
        return '-'.join(data.strip().split('-')[:-2])