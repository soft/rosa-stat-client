import ctypes


class SerializableStructure(ctypes.Structure):
    def serialize(self):
        return buffer(self)[:]

    @classmethod
    def deserialize(cls, data):
        struct = cls()
        ctypes.memmove(ctypes.addressof(struct), data, ctypes.sizeof(struct))
        return struct


class fanotify_event_metadata(SerializableStructure):
    _fields_ = [
        ('event_len', ctypes.c_uint32), ('vers', ctypes.c_uint8), ('reserved', ctypes.c_uint8),
        ('metadata_len', ctypes.c_uint16), ('mask', ctypes.c_uint64), ('fd', ctypes.c_int32), ('pid', ctypes.c_int32)
    ]


class fanotify_response(SerializableStructure):
    _fields_ = [
        ('fd', ctypes.c_int32), ('response', ctypes.c_uint32)
    ]