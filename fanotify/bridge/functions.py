import ctypes
import ctypes.util
import errno as _errno
from os import O_RDONLY


clib = ctypes.CDLL(ctypes.util.find_library('c'), use_errno=True)

fanotify_init_native = clib.fanotify_init
fanotify_init_native.argtypes = [ctypes.c_uint, ctypes.c_uint]
fanotify_init_native.restype = ctypes.c_int

fanotify_mark_native = clib.fanotify_mark
fanotify_mark_native.argtypes = [ctypes.c_int, ctypes.c_uint, ctypes.c_uint64, ctypes.c_int, ctypes.c_char_p]
fanotify_mark_native.restype = ctypes.c_int


def fanotify_init(flags, event_flags=O_RDONLY):
    """Create a fanotify handle"""

    assert isinstance(flags, int), 'Flags must be an integer'
    assert isinstance(event_flags, int), 'Event flags must be an integer'

    fd = fanotify_init_native(flags, event_flags)
    if fd < 0:
        err = ctypes.get_errno()
        if err == _errno.EINVAL:
            raise ValueError("Invalid argument or flag")
        elif err == _errno.EMFILE:
            raise OSError("Maximum fanotify instances reached")
        elif err == _errno.ENOMEM:
            raise MemoryError("Insufficent kernel memory avalible")
        elif err == _errno.EPERM:
            raise OSError("Operation not permitted")
        elif err == _errno.ENOSYS:
            raise OSError("This kernel does not support fanotify API")
        else:
            # If you are here, its a bug. send us the traceback
            raise ValueError("Unknown Error: {}".format(err))

    return fd


def fanotify_mark(fd, flags, mask, path, dfd=0):
    """Add a file to a fanotify context"""

    assert isinstance(fd, int), 'FD must be an integer'
    assert isinstance(dfd, int), 'DFD must be an integer'
    assert isinstance(flags, int), 'Flags must be an integer'
    assert isinstance(mask, int), 'Mask must be an integer'
    assert isinstance(path, (str, bytes)), 'Path must be a string'

    if isinstance(path, str):
        path = path.encode()

    ret = fanotify_mark_native(fd, flags, mask, dfd, path)
    if ret < 0:
        err = ctypes.get_errno()
        if err == _errno.EINVAL:
            raise ValueError("Invalid flag or mask")
        elif err == _errno.EBADF:
            raise OSError("fd does not exist or was of the incorrect type")
        elif err == _errno.ENOENT:
            raise OSError("File system object is invalid or directory/mount not marked")
        elif err == _errno.ENOMEM:
            raise MemoryError("Insufficent kernel memory avalible")
        elif err == _errno.ENOSPC:
            raise OSError("Too many marks")
        elif err == _errno.ENOSYS:
            raise OSError("This kernel does not support fanotify API")
        elif err == _errno.ENOTDIR:
            raise ValueError("Directory must be specified")
        else:
            # If you are here, its a bug. send us the traceback
            raise ValueError("Unknown Error: {}".format(err))