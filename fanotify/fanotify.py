"""fanotify: wrapper around the fanotify family of syscalls for watching for file modifcation"""
import ctypes
from os import getpid as _getpid, readlink as _readlink
from os import close as _close
from os import read as _read, write as _write
from os import O_RDONLY, O_WRONLY, O_RDWR
from os.path import join as _path_join

from .utils import get_buffered_length as _get_buffered_length
from .utils import Eventlike as _Eventlike

from .bridge import *


READ_EVENTS_MAX = 10


class Fanotify(_Eventlike):
    blocking = True

    def __init__(self, flags=FAN_CLASS_NOTIF, event_flags=O_RDONLY):
        super(self.__class__, self).__init__()
        self._fd = fanotify_init(flags, event_flags)

        self._events = []

        if flags & FAN_NONBLOCK:
            self.blocking = False

        if event_flags & O_RDWR | O_WRONLY:
            self._mode = 'w+'
        elif event_flags & O_WRONLY:
            self._mode = 'w'
        else:
            self._mode = 'r'

    def watch(self, flags, mask, path, dfd=0):
        flags |= FAN_MARK_ADD
        fanotify_mark(self.fileno(), flags, mask, path, dfd)

    def del_watch(self, flags, mask, path, dfd=0):
        self.ignore(flags, mask, path, dfd)

    def ignore(self, flags, mask, path, dfd=0):
        flags |= FAN_MARK_REMOVE
        fanotify_mark(self.fileno(), flags, mask, path, dfd)

    def _read_events(self):
        fd = self.fileno()

        event_struct_size = ctypes.sizeof(fanotify_event_metadata)

        buf_len = min(_get_buffered_length(fd), event_struct_size * READ_EVENTS_MAX)
        raw_events = _read(fd, buf_len)

        events = FanotifyEvent.str_to_events(raw_events)

        return events

    def write_response(self, event, response):
        assert isinstance(event, FanotifyEvent), 'event must be a FanotifyEvent class instance'
        assert isinstance(response, int), 'Response must be an integer'

        response_struct = fanotify_response(event.fd, response)
        buf = response_struct.serialize()
        _write(self.fileno(), buf)


class FanotifyEvent(object):
    __slots__ = ['_filename', 'version', 'mask', 'fd', 'pid']

    def __init__(self, version, mask, fd, pid):
        self.version = version
        self.mask = mask
        self.fd = fd
        self.pid = pid

        self._filename = None

    @staticmethod
    def str_to_events(buf):
        event_struct_size = ctypes.sizeof(fanotify_event_metadata)

        events = []

        i = 0
        while i < len(buf):
            event = fanotify_event_metadata.deserialize(buf[i:i + event_struct_size])
            events.append(FanotifyEvent(event.vers, event.mask, event.fd, event.pid))

            i += event.event_len

        return events

    @property
    def filename(self):
        if not self._filename:
            try:
                name = _readlink(_path_join('/proc', str(_getpid()), 'fd', str(self.fd)))
                self._filename = name
            except OSError:
                self._filename = "<Unknown>"

        return self._filename

    def close(self):
        _close(self.fd)
        self.fd = None

    def __repr__(self):
        return "<FanotifyEvent filename={}, version={}, mask=0x{:X}, fd={}, pid={}>".format(
            self.filename, self.version, self.mask, self.fd, self.pid)

    @property
    def access_event(self):
        return True if self.mask & FAN_ACCESS else False

    @property
    def access_perm_event(self):
        return True if self.mask & FAN_ACCESS_PERM else False

    @property
    def modify_event(self):
        return True if self.mask & FAN_MODIFY else False

    @property
    def close_event(self):
        return True if self.mask & FAN_CLOSE else False

    @property
    def close_write_event(self):
        return True if self.mask & FAN_CLOSE_WRITE else False

    @property
    def close_nowrite_event(self):
        return True if self.mask & FAN_CLOSE_NOWRITE else False

    @property
    def open_event(self):
        return True if self.mask & FAN_OPEN else False

    @property
    def open_perm_event(self):
        return True if self.mask & FAN_OPEN_PERM else False

    @property
    def queue_overflow_event(self):
        return True if self.mask & FAN_Q_OVERFLOW else False

    @property
    def on_dir_event(self):
        return True if self.mask & FAN_ONDIR else False

    @property
    def on_child_event(self):
        return True if self.mask & FAN_EVENT_ON_CHILD else False