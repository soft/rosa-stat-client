#!/bin/bash

start() {
    /usr/bin/python main.py
    while [ ! -f rosa_stat.pid ]
    do
        :
    done
}

stop() {
    kill -KILL `cat /tmp/rosa_stat.pid`
}

case "$1" in
start)
    start
;;
stop)
    stop
;;
restart)
    stop
    start
;;
*)
    echo start, stop or restart
;;
esac
